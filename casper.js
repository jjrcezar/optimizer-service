var phantomcss = require('phantomcss');

var arg_url = casper.cli.get("url");
var arg_left = casper.cli.get("left");
var arg_top = casper.cli.get("top");
var arg_width = casper.cli.get("width");
var arg_height = casper.cli.get("height");
var arg_viewport_width = casper.cli.get("viewportWidth");
var arg_viewport_height = casper.cli.get("viewportHeight");
var arg_id = casper.cli.get("id");

casper.test.begin('CSS Regression Test', function (test) {
  phantomcss.init({
    rebase: casper.cli.get('rebase'),
    screenshotRoot: './tmp/cssregression/' + arg_id,
    failedComparisonsRoot: false,
    addIteratorToImage: false,
    onPass: function (test) { },
    onFail: function (test) { },
    onTimeout: function () { },
    onNewImage: function () { },
    onComplete: function (allTests, noOfFails, noOfErrors) {
      var mismatch = -1;
      allTests.forEach(function (test) {
        mismatch = test.mismatch;
      });
      console.log(mismatch || 0);
    }
  });
  casper.start(arg_url);
  casper.viewport(arg_viewport_width, arg_viewport_height);
  casper.then(function () {
    phantomcss.screenshot({
      left: arg_left,
      top: arg_top,
      width: arg_width,
      height: arg_height
    }, arg_id);
  });
  casper.then(function compare_diffs() {
    phantomcss.compareAll();
  });
});

casper.run(function () {
  casper.test.done();
});

import { NestFactory } from '@nestjs/core';
import { ApplicationModule } from './modules/application/app.module';
import express = require('express');
import * as cors from 'cors';
import * as bodyParser from 'body-parser';

var whitelist = ['https://swagger.novanus.com'];
var corsOptions = {
    // origin: function (origin, callback) {
    //     console.log("ORIGIN", origin);
    //     if (!origin || whitelist.indexOf(origin) !== -1) {
    //         callback(null, true);
    //     } else {
    //         callback(new Error('Request not allowed'));
    //     }
    // }
    "origin":"*"
};

const expressInstance = express();
expressInstance.use(bodyParser.json());
expressInstance.use(cors(corsOptions));

const app = NestFactory.create(ApplicationModule, expressInstance);
app.setGlobalPrefix('optimizer/v1.0');
app.listen(3000, () => console.log('Application is listening on port 3000.'));

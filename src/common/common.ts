export class ResponseObj {
    url: string;
    extracted: string;
}

export class Constants {
    static TMP_DIR = "tmp/";
    static TMP_CSS_FILE = "tmpstyle.css";
    static TMP_CSS_REGRESSION_PATH = "tmp/cssregression";
    static KEY_AUTHORIZED_DOMAINS = "auth_domains";
    static KEY_SCREENSHOT_IDS = "screenshot_ids";
    static BAD_REQUEST_INTERNAL_ERROR_MSG: string = "ERROR: An internal error occured.";
    static BAD_REQUEST_INVALID_PARAMS_MSG: string = "ERROR: Invalid parameters.";
    static SUCCESS_MSG: string = "SUCCESS: Operation successfull."
    static FAIL_MSG: string = "FAILED: Operation unsuccessfull."
    static UNAUTHORIZED_URL_MSG: string = "Unauthorized URL(s) found in parameters."
    static SCRIPTS_SCHEMA = {
        "id": "/ScriptsOptionsSchema",
        "type": "object",
        "properties": {
            "url": {
                "type": "array",
                "items": { "type": "string" }
            },
            "options": {
                "type": "object",
                "properties": {
                    "mangle": {
                        "type": ["object", "boolean"],
                        "properties": {
                            "reserved": {
                                "type": "array",
                                "items": { "type": "string" }
                            },
                            "toplevel": { "type": "boolean" },
                            "keep_fnames": { "type": "boolean" },
                            "eval": { "type": "boolean" }
                        },
                        "additionalProperties": false
                    },
                    "compress": {
                        "type": ["object", "boolean"],
                        "properties": {
                            "sequences": { "type": ["boolean", "number"] },
                            "properties": { "type": "boolean" },
                            "dead_code": { "type": "boolean" },
                            "drop_debugger": { "type": "boolean" },
                            "conditionals": { "type": "boolean" },
                            "comparisons": { "type": "boolean" },
                            "booleans": { "type": "boolean" },
                            "typeofs": { "type": "boolean" },
                            "loops": { "type": "boolean" },
                            "unused": { "type": "boolean" },
                            "toplevel": { "type": "boolean" },
                            "if_return": { "type": "boolean" },
                            "inline": { "type": "boolean" }
                        },
                        "additionalProperties": false
                    }
                },
                "additionalProperties": false
            }
        },
        "required": ["url"],
        "additionalProperties": false
    };
    static STYLES_SCHEMA = {
        "id": "/StylesOptionsSchema",
        "type": "object",
        "properties": {
            "url": {
                "type": "array",
                "items": { "type": "string" }
            },
            "options": {
                "type": "object",
                "properties": {
                    "level": { "type": "number" },
                    "format": {
                        "type": "string",
                        "enum": ["beautify", "keep-breaks"]
                    }
                },
                "additionalProperties": false
            }
        },
        "required": ["url"],
        "additionalProperties": false
    };
    static PURIFYCSS_SCHEMA = {
        "id": "/PurifyCSSOptionsSchema",
        "type": "object",
        "properties": {
            "contentUrl": { "type": "string" },
            "cssUrl": { "type": "string" },
            "options": {
                "type": "object",
                "properties": {
                    "minify": { "type": "boolean" },
                    "info": { "type": "boolean" },
                    "rejected": { "type": "boolean" },
                    "whitelist": {
                        "type": "array",
                        "items": { "type": "string" }
                    }
                },
                "additionalProperties": false
            }
        },
        "required": ["contentUrl", "cssUrl"],
        "additionalProperties": false
    };
    static CRITICALCSS_SCHEMA = {
        "id": "/CriticalCSSOptionsSchema",
        "type": "object",
        "properties": {
            "contentUrl": { "type": "string" },
            "cssUrl": { "type": "string" },
            "options": {
                "type": "object",
                "properties": {
                    "dimensions": {
                        "type": "array",
                        "items": {
                            "type": "object",
                            "properties": {
                                "height": { "type": "number" },
                                "width": { "type": "number" },
                            },
                            "additionalProperties": false
                        }
                    },
                    "timeout": { "type": "number" },
                    "minify": { "type": "boolean" },
                    "ignore": {
                        "type": "array",
                        "items": { "type": "string" }
                    },
                    "ignoreOptions": {
                        "type": "object",
                        "properties": {
                            "matchSelectors": { "type": "boolean" },
                            "matchTypes": { "type": "boolean" },
                            "matchDeclarationProperties": { "type": "boolean" },
                            "matchDeclarationValues": { "type": "boolean" },
                            "matchMedia": { "type": "boolean" },
                        },
                        "additionalProperties": false
                    }
                },
                "additionalProperties": false
            }
        },
        "required": ["contentUrl", "cssUrl", "options"],
        "additionalProperties": false
    };
    static ADD_DOMAIN_SCHEMA = {
        "id": "/AddDomainSchema",
        "type": "object",
        "properties": {
            "domain": {
                "type": "array",
                "items": { "type": "string" }
            }
        },
        "required": ["domain"],
        "additionalProperties": false

    };
    static DELETE_DOMAIN_SCHEMA = {
        "id": "/DeleteDomainSchema",
        "type": "object",
        "properties": {
            "domain": {
                "type": "array",
                "items": { "type": "string" }
            }
        },
        "required": ["domain"],
        "additionalProperties": false
    };
    static CSSREGRESSION_SCHEMA = {
        "id": "/CSSRegressionSchema",
        "type": "object",
        "properties": {
            "url": { "type": "string" },
            "id": { "type": "string" },
            "options": {
                "type": "object",
                "properties": {
                    "top": { "type": "number" },
                    "left": { "type": "number" },
                    "width": { "type": "number" },
                    "height": { "type": "number" },
                    "viewportWidth": { "type": "number" },
                    "viewportHeight": { "type": "number" }
                },
                "required": ["top", "left", "width", "height", "viewportWidth", "viewportHeight"],
                "additionalProperties": false
            }
        },
        "required": ["url", "options"],
        "additionalProperties": false
    };
}

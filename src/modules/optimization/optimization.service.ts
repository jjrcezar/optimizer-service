import { Component } from '@nestjs/common';
import { HttpService } from './http.service';
import { Constants } from '../../common/common';
import * as CleanCSS from 'clean-css';
import * as CleanJS from 'uglify-js';
import * as PurifyCSS from 'purify-css';
import * as CriticalCSS from 'critical';
import * as FileSystem from 'fs';

@Component()
export class OptimizationService {

    constructor(private httpService: HttpService) { }

    public async optimizeScript(url: string, options: any): Promise<string> {
        const js = await this.httpService.retrieveUrl(url);
        var min = CleanJS.minify(js, options);
        return min.code || (min.error && min.error.message) || '';
    }

    public async optimizeStyle(url: string, options: any): Promise<string> {
        const css = await this.httpService.retrieveUrl(url);
        var min = new CleanCSS(options).minify(css);
        return min.styles || JSON.stringify(min.errors);
    }

    public async purifyCss(contentUrl: any, cssUrl: any, options: any): Promise<string> {
        const html = await this.httpService.retrieveUrl(contentUrl);
        const styles = await this.httpService.retrieveUrl(cssUrl);
        return PurifyCSS(html, styles, options);
    }

    public async criticalCss(contentUrl: any, cssUrl: any, options: any): Promise<string> {
        const html = await this.httpService.retrieveUrl(contentUrl);
        const styles = await this.httpService.retrieveUrl(cssUrl);

        let tempCssFilepath = `${Constants.TMP_DIR}${Constants.TMP_CSS_FILE}`;
        if (!FileSystem.existsSync(Constants.TMP_DIR)) {
            FileSystem.mkdirSync(Constants.TMP_DIR);
        }
        FileSystem.writeFileSync(tempCssFilepath, styles);

        options.base = Constants.TMP_DIR;
        options.html = html;
        options.css = [tempCssFilepath];

        return CriticalCSS.generate(options);
    }
}
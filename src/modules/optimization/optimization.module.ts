import { Module, NestModule } from '@nestjs/common';
import { OptimizationController } from './optimization.controller';
import { OptimizationService } from './optimization.service';
import { HttpService } from './http.service';

@Module({
    controllers: [
        OptimizationController
    ],
    components: [
        OptimizationService,
        HttpService
    ]
})
export class OptimizationModule implements NestModule {

}
import { Controller, Get, HttpStatus, Post, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express';
import { OptimizationService } from './optimization.service';
import { ResponseObj, Constants } from '../../common/common';
import { Validator } from 'jsonschema';
import * as ChildProcess from 'child_process';
import * as Scheduler from 'node-schedule';
import * as FindRemove from 'find-remove';
import * as Storage from 'node-persist';
import * as FileSystem from 'fs';
import * as Path from 'path';
import * as URL from 'url';

@Controller()
export class OptimizationController {

    private validator = new Validator();

    constructor(private optimizationService: OptimizationService) {
        Storage.init({
            dir: `${Constants.TMP_DIR}/.node-persist`
        });
        Scheduler.scheduleJob('* */15 * * * *', function () {
            FindRemove(Path.resolve(`${__dirname}/../../../${Constants.TMP_CSS_REGRESSION_PATH}`),
                { age: { seconds: 3600 }, extensions: '.png', dir: '*' });
        });
    }

    @Post('scripts')
    public async getScripts( @Req() request: Request, @Res() response: Response) {
        var promises = [];
        var result: any = [];

        let validatorResult = this.validator.validate(request.body, Constants.SCRIPTS_SCHEMA);

        if (validatorResult.valid) {
            if (this._validatedomain(request.body.url)) {
                request.body.url.forEach(url => {
                    promises.push(this.optimizationService.optimizeScript(url, request.body.options)
                        .then(opt => {
                            var item = new ResponseObj;
                            item.url = url;
                            item.extracted = opt;
                            result.push(item);
                        }));
                });
                Promise.all(promises)
                    .then(() => response.status(HttpStatus.OK).send(result))
                    .catch(err => response.status(HttpStatus.BAD_REQUEST).send(
                        {
                            status: Constants.BAD_REQUEST_INTERNAL_ERROR_MSG,
                            message: err
                        }));
            } else {
                response.status(HttpStatus.BAD_REQUEST).send(
                    {
                        status: Constants.BAD_REQUEST_INVALID_PARAMS_MSG,
                        message: Constants.UNAUTHORIZED_URL_MSG
                    });
            }
        } else {
            response.status(HttpStatus.BAD_REQUEST).send(
                {
                    status: Constants.BAD_REQUEST_INVALID_PARAMS_MSG,
                    message: validatorResult.errors.map(err => { return `${err.property}: ${err.message}` }).join(", ")
                });
        }
    }

    @Post('styles')
    public async getStyles( @Req() request: Request, @Res() response: Response) {
        var promises = [];
        var result: any = [];

        let validatorResult = this.validator.validate(request.body, Constants.STYLES_SCHEMA);

        if (validatorResult.valid) {
            if (this._validatedomain(request.body.url)) {
                request.body.url.forEach(url => {
                    promises.push(this.optimizationService.optimizeStyle(url, request.body.options)
                        .then(opt => {
                            var item = new ResponseObj;
                            item.url = url;
                            item.extracted = opt;
                            result.push(item);
                        }));
                });
                Promise.all(promises)
                    .then(() => response.status(HttpStatus.OK).send(result))
                    .catch(err => response.status(HttpStatus.BAD_REQUEST).send(
                        {
                            status: Constants.BAD_REQUEST_INTERNAL_ERROR_MSG,
                            message: err
                        }));
            } else {
                response.status(HttpStatus.BAD_REQUEST).send(
                    {
                        status: Constants.BAD_REQUEST_INVALID_PARAMS_MSG,
                        message: Constants.UNAUTHORIZED_URL_MSG
                    });
            }
        } else {
            response.status(HttpStatus.BAD_REQUEST).send(
                {
                    status: Constants.BAD_REQUEST_INVALID_PARAMS_MSG,
                    message: validatorResult.errors.map(err => { return `${err.property}: ${err.message}` }).join(", ")
                });
        }
    }

    @Post('purifycss')
    public async purifyCss( @Req() request: Request, @Res() response: Response) {

        let validatorResult = this.validator.validate(request.body, Constants.PURIFYCSS_SCHEMA);

        if (validatorResult.valid) {
            const { contentUrl, cssUrl, options } = request.body;
            if (this._validatedomain([contentUrl, cssUrl])) {
                this.optimizationService.purifyCss(contentUrl, cssUrl, options)
                    .then(purified => response.status(HttpStatus.OK).send(purified))
                    .catch(err => response.status(HttpStatus.BAD_REQUEST).send(
                        {
                            status: Constants.BAD_REQUEST_INTERNAL_ERROR_MSG,
                            message: err
                        }));
            } else {
                response.status(HttpStatus.BAD_REQUEST).send(
                    {
                        status: Constants.BAD_REQUEST_INVALID_PARAMS_MSG,
                        message: Constants.UNAUTHORIZED_URL_MSG
                    });
            }
        } else {
            response.status(HttpStatus.BAD_REQUEST).send(
                {
                    status: Constants.BAD_REQUEST_INVALID_PARAMS_MSG,
                    message: validatorResult.errors.map(err => { return `${err.property}: ${err.message}` }).join(", ")
                });
        }
    }

    @Post('criticalcss')
    public async criticalCss( @Req() request: Request, @Res() response: Response) {

        let validatorResult = this.validator.validate(request.body, Constants.CRITICALCSS_SCHEMA);

        if (validatorResult.valid) {
            const { contentUrl, cssUrl, options } = request.body;
            if (this._validatedomain([contentUrl, cssUrl])) {
                if (options.minify === undefined) {
                    options.minify = true;
                }
                this.optimizationService.criticalCss(contentUrl, cssUrl, options)
                    .then(result => response.status(HttpStatus.OK).send(result))
                    .catch(err => response.status(HttpStatus.BAD_REQUEST).send(
                        {
                            status: Constants.BAD_REQUEST_INTERNAL_ERROR_MSG,
                            message: err
                        }));
            } else {
                response.status(HttpStatus.BAD_REQUEST).send(
                    {
                        status: Constants.BAD_REQUEST_INVALID_PARAMS_MSG,
                        message: Constants.UNAUTHORIZED_URL_MSG
                    });
            }
        } else {
            response.status(HttpStatus.BAD_REQUEST).send(
                {
                    status: Constants.BAD_REQUEST_INVALID_PARAMS_MSG,
                    message: validatorResult.errors.map(err => { return `${err.property}: ${err.message}` }).join(", ")
                });
        }
    }

    @Post('adddomain')
    public async addDomain( @Req() request: Request, @Res() response: Response) {

        let validatorResult = this.validator.validate(request.body, Constants.ADD_DOMAIN_SCHEMA);
        if (validatorResult.valid) {
            let items = 0;
            let auth_domains = Storage.getItemSync(Constants.KEY_AUTHORIZED_DOMAINS) || [];
            request.body.domain.forEach(domain => {
                if (auth_domains.indexOf(domain) === -1) {
                    auth_domains.push(domain)
                    Storage.setItemSync(Constants.KEY_AUTHORIZED_DOMAINS, auth_domains);
                    items++;
                }
            });
            response.status(items ? HttpStatus.OK : HttpStatus.BAD_REQUEST).send({
                status: items ? Constants.SUCCESS_MSG : Constants.FAIL_MSG,
                message: `Added ${items} items.`
            });
        } else {
            response.status(HttpStatus.BAD_REQUEST).send(
                {
                    status: Constants.BAD_REQUEST_INVALID_PARAMS_MSG,
                    message: validatorResult.errors.map(err => { return `${err.property}: ${err.message}` }).join(", ")
                });
        }
    }

    @Post('deletedomain')
    public async deleteDomain( @Req() request: Request, @Res() response: Response) {

        let validatorResult = this.validator.validate(request.body, Constants.DELETE_DOMAIN_SCHEMA);
        if (validatorResult.valid) {
            let items = 0;
            let auth_domains = Storage.getItemSync(Constants.KEY_AUTHORIZED_DOMAINS) || [];
            request.body.domain.forEach(domain => {
                let index = auth_domains.indexOf(domain);
                if (index !== -1) {
                    auth_domains.splice(index, 1);
                    Storage.setItemSync(Constants.KEY_AUTHORIZED_DOMAINS, auth_domains);
                    items++;
                }
            });
            response.status(items ? HttpStatus.OK : HttpStatus.BAD_REQUEST).send({
                status: items ? Constants.SUCCESS_MSG : Constants.FAIL_MSG,
                message: `Deleted ${items} items.`
            });
        } else {
            response.status(HttpStatus.BAD_REQUEST).send(
                {
                    status: Constants.BAD_REQUEST_INVALID_PARAMS_MSG,
                    message: validatorResult.errors.map(err => { return `${err.property}: ${err.message}` }).join(", ")
                });
        }
    }

    @Post('cssregression')
    public async cssRegression( @Req() request: Request, @Res() response: Response) {
        let validatorResult = this.validator.validate(request.body, Constants.CSSREGRESSION_SCHEMA);
        if (validatorResult.valid) {
            if (this._validatedomain([request.body.url])) {
                let id = request.body.id || (Math.random() + 1).toString(36).substring(7);
                const { top, left, width, height, viewportWidth, viewportHeight } = request.body.options;
                var exec = ChildProcess.exec(`casperjs test --verbose=false --log-level=error --concise --no-colors casper.js --url=${request.body.url} --top=${top} --left=${left} --width=${width} --height=${height} --viewportWidth=${viewportWidth} --viewportHeight=${viewportHeight} --id=${id}`,
                    function (error, stdout, stderr) {
                        if (!stderr) {
                            let temp = stdout.split(/(\r?\n)+/);
                            let mismatch = stdout.split(/(\r?\n)+/)[1];
                            if (mismatch !== "-1") {
                                var results = {
                                    mismatch: mismatch,
                                    baseImageUrl: `${Constants.TMP_CSS_REGRESSION_PATH}/${id}/${id}.png`,
                                    diffImageUrl: `${Constants.TMP_CSS_REGRESSION_PATH}/${id}/${id}.diff.png`
                                };
                                if (mismatch !== "0") {
                                    results["failImageUrl"] = `${Constants.TMP_CSS_REGRESSION_PATH}/${id}/${id}.fail.png`;
                                }
                                response.status(HttpStatus.OK).send({ results, stdout: stdout, mismatch: mismatch, temp });
                            } else {
                                response.status(HttpStatus.OK).send({ id: id, stdout: stdout, mismatch: mismatch, temp });
                            }
                        } else {
                            response.status(HttpStatus.BAD_REQUEST).send({
                                status: Constants.BAD_REQUEST_INTERNAL_ERROR_MSG,
                                message: stderr
                            });
                        }
                    });
            } else {
                response.status(HttpStatus.BAD_REQUEST).send(
                    {
                        status: Constants.BAD_REQUEST_INVALID_PARAMS_MSG,
                        message: Constants.UNAUTHORIZED_URL_MSG
                    });
            }
        } else {
            response.status(HttpStatus.BAD_REQUEST).send(
                {
                    status: Constants.BAD_REQUEST_INVALID_PARAMS_MSG,
                    message: validatorResult.errors.map(err => { return `${err.property}: ${err.message}` }).join(", ")
                });
        }
    }

    private _validatedomain(urls: any[]): boolean {
        let allowedDomains = Storage.getItemSync(Constants.KEY_AUTHORIZED_DOMAINS);
        return urls.filter(url => allowedDomains.indexOf(URL.parse(url).hostname) === -1).length === 0;
    }

    @Post('getfile')
    public async getFile( @Req() request: Request, @Res() response: Response) {
        response.status(HttpStatus.OK).sendFile(Path.resolve(`${__dirname}/../../../${request.body.url}`));
    }

    @Get('listdomain')
    public async listDomain( @Req() request: Request, @Res() response: Response) {
        response.status(HttpStatus.OK).send(Storage.getItemSync(Constants.KEY_AUTHORIZED_DOMAINS) || []);
    }

    @Get('samplejs')
    public async getSampleJS( @Req() request: Request, @Res() response: Response) {
        response.status(HttpStatus.OK).sendFile('C:/Users/JSP/Documents/Freelance/Novanus/Source/optimizer.service/src/dummy.js');
    }

    @Get('samplecss')
    public async getSampleCSS( @Req() request: Request, @Res() response: Response) {
        response.status(HttpStatus.OK).sendFile('C:/Users/JSP/Documents/Freelance/Novanus/Source/optimizer.service/src/dummy.css');
    }

    @Get('samplepurifyhtml')
    public async getSamplePurifyHTML( @Req() request: Request, @Res() response: Response) {
        response.status(HttpStatus.OK).sendFile('C:/Users/JSP/Documents/Freelance/Novanus/Source/optimizer.service/src/purify.html');
    }

    @Get('samplepurifycss')
    public async getSamplePurifyCSS( @Req() request: Request, @Res() response: Response) {
        response.status(HttpStatus.OK).sendFile('C:/Users/JSP/Documents/Freelance/Novanus/Source/optimizer.service/src/purify.css');
    }

    @Get('samplecriticalcss')
    public async getSampleCriticalCSS( @Req() request: Request, @Res() response: Response) {
        response.status(HttpStatus.OK).sendFile('C:/Users/JSP/Documents/Freelance/Novanus/Source/optimizer.service/src/critical.html');
    }

    @Get('pipelinetest')
    public async getPipelineTest( @Req() request: Request, @Res() response: Response) {
        response.status(HttpStatus.OK).send('Pipeline Test OK');
    }
}
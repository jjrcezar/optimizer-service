import { Component } from '@nestjs/common';
import fetch from 'node-fetch';

@Component()
export class HttpService {
    public retrieveUrl(url: string): Promise<string> {
        return fetch(url)
            .then(res => {
                return res.text();
            }).catch(error => {
                console.log(error);
                throw error;
            });
    }
}
import { Module } from '@nestjs/common';
import { OptimizationModule } from '../optimization/optimization.module';

@Module({
    modules: [OptimizationModule],
})
export class ApplicationModule { }